﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orb : MonoBehaviour
{ 

    public float detectSize = 5f, speedToPlayer = 1f;
    public bool foundPlayer = false;

    GameObject player;

    Rigidbody2D orbRB;

    public playerData.statTypes statType = new playerData.statTypes();


    void Start()
    {
        string[] s = System.Enum.GetNames(typeof(playerData.statTypes));
        statType = (playerData.statTypes)System.Enum.Parse(typeof(playerData.statTypes), s[Random.Range(0, s.Length)]);


        CircleCollider2D orbDetect = gameObject.AddComponent<CircleCollider2D>();
        orbDetect.radius = detectSize;
        orbDetect.isTrigger = true;
        orbRB = this.gameObject.AddComponent<Rigidbody2D>();
        orbRB.gravityScale = 0;

        player = GameObject.FindGameObjectWithTag("Player");
    }
    void Update()
    {

        if (foundPlayer)
        {
            Vector3 diff = player.transform.position - transform.position;
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

            orbRB.AddRelativeForce(Vector2.up * speedToPlayer, ForceMode2D.Force);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag=="Player")
        {

            foundPlayer = true;

        }
    }
}
