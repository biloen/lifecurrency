﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gameUIController : MonoBehaviour
{
    List<GameObject> statTexts = new List<GameObject>();

    void Start()
    {
        foreach (string s in System.Enum.GetNames(typeof(playerData.statTypes)))
        {
            statTexts.Add(new GameObject(s));
        }


        foreach (GameObject i in statTexts)
        {
            i.layer = 5;
            i.transform.SetParent(GameObject.Find("Canvas").transform);
            RectTransform RT = i.AddComponent<RectTransform>();
            RT.sizeDelta = new Vector2(Screen.width,60);

            i.AddComponent<CanvasRenderer>();

            Text t = i.AddComponent<Text>();
            t.text = " "+i.gameObject.name+":";
            t.fontSize = 20;
            t.alignment = TextAnchor.LowerLeft;
            t.color = Color.black;
            t.font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        }

        for (int i = 0; i < statTexts.Count; i++)
        {
            statTexts[i].GetComponent<RectTransform>().position = new Vector3(Screen.width / 2, Screen.height-(i*20), 0);
        }
    }

    void Update()
    {
        string[] s = System.Enum.GetNames(typeof(playerData.statTypes));
        for (int i = 0; i < s.Length; i++)
        {
            // ik kig på den her kode hvis du ikke vil have hovedpine
            statTexts[i].GetComponent<Text>().text = " " + statTexts[i].name + ": " + playerData.stats[(playerData.statTypes)System.Enum.Parse(typeof(playerData.statTypes), s[i])].ToString();
        }
    }
}
