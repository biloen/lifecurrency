﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public static class playerData
{

    public enum statTypes
    {
        strength,
        dexterity,
        intelligence,
    }


    public enum weapons
    {
        sword = 0,
        pistol = 1,
        rifle = 2
    }

    public static Dictionary<statTypes, int> stats = new Dictionary<statTypes, int>(); 
    public static Dictionary<weapons, bool> aquiredWeapons = new Dictionary<weapons, bool>();


}
