﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class initPlayerData : MonoBehaviour
{
    void Start()
    {
        DontDestroyOnLoad(gameObject);

        // init all stats as 1
        foreach (playerData.statTypes i in System.Enum.GetValues(typeof(playerData.statTypes)))
        {
            playerData.stats.Add(i, 1);
        }

        // and all weapons as not aquired yet
        foreach (playerData.weapons i in System.Enum.GetValues(typeof(playerData.weapons)))
        {
            playerData.aquiredWeapons.Add(i, false);
        }

        // give player starting weapon
        playerData.aquiredWeapons[playerData.weapons.sword] =  true;
        playerData.aquiredWeapons[playerData.weapons.pistol] = true;

    }
}
