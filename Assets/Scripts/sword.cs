﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sword : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject i in GameObject.FindGameObjectsWithTag("Weapon"))
        {
            if (i.name != gameObject.name)
            {
                Destroy(i);
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
