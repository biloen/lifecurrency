﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour
{
    public float moveSpeed = 5f, jumpForce = 100f;

    Rigidbody2D playerBody;
    void Start()
    {
        playerBody = gameObject.GetComponent<Rigidbody2D>();
        playerBody.constraints = RigidbodyConstraints2D.FreezeRotation;

        initPlayerData();
    }
    private void FixedUpdate()
    {
        Move();

    }
    void Update()
    {
        Jump();
    }

    void Jump()
    {
        if (Input.GetKeyDown("space")==true&&!isGrounded())
        {
            playerBody.velocity = Vector2.up * jumpForce;
        }
    }
    void Move()
    {
        if (Input.GetKey(KeyCode.A))
        {
            playerBody.velocity = new Vector2(-moveSpeed, playerBody.velocity.y);
        }
        else
        {
            if (Input.GetKey(KeyCode.D))
            {
                playerBody.velocity = new Vector2(moveSpeed, playerBody.velocity.y);
            }
            else
            {
                playerBody.velocity = new Vector2(0, playerBody.velocity.y);
            }
        } 

    }
    private bool isGrounded()
    {
        return false;
    }

    void initPlayerData()
    {
        bool playerdataInitialized;

        if (FindObjectsOfType<initPlayerData>().Length != 1)
        {
            GameObject go = new GameObject("initPlayerData");
            go.AddComponent<initPlayerData>();
        }
    }
}
